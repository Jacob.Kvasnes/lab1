package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
    
    public static void main(String[] args) {
        /* 	
        * The code here does two things:
        * It first creates a new RockPaperScissors -object with the
        * code `new RockPaperScissors()`. Then it calls the `run()`
        * method on the newly created object.
        */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        Random rand = new Random();
        String humanChoice;
        
        //rand.nextInt(3)
        while(true){
            String winner = "Human wins!";
            System.out.println("Let's play round "+roundCounter);
            while(true){
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if (rpsChoices.indexOf(humanChoice) != -1){
                    break;
                } else{
                    System.out.println("I do not understand "+humanChoice+". Could you try again?");
                }
            }
            String computerChoice = rpsChoices.get(rand.nextInt(3));
            
            if(humanChoice.equals(computerChoice)){
                winner = "It's a tie!";
            }else if((humanChoice.equals("rock") && computerChoice.equals("paper")) || (humanChoice.equals("paper") && computerChoice.equals("scissors")) || (humanChoice.equals("scissors") && computerChoice.equals("rock"))){
                winner = "Computer wins!";
                computerScore++;
            }else{
                humanScore++;
            }
            
            System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". "+winner);
            System.out.println("Score: human "+humanScore+", computer "+computerScore);

            while(true){
                String rematch = readInput("Do you wish to continue playing? (y/n)?");
                if (rematch.equals("n")){
                    System.out.println("Bye bye :)");
                    return;
                } else if(rematch.equals("y")){
                    roundCounter++;
                    break;
                } else {
                    System.out.println("I do not understand "+rematch+". Could you try again?");
                }

            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
